package ch03;

public class Operation2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long l = 10000L * 10000L;
		System.out.println(l);
		
		int a = 10;
		int b = 5;
		int result = ++a + b++;
		System.out.println(result);
		System.out.println(a);
		System.out.println(b);
		
		int c = 10;
		int d = 20;
		boolean result2 = c > d;
		System.out.println(result2);
		
		result2 = c != d;
		System.out.println(result2);
		
		result2 = (c + 10) != d;
		System.out.println(result2);
		
		int share = 10 / 8;
		int remain = 10 % 8;
		
		System.out.println(share);
		System.out.println(remain);
		
	}

}
