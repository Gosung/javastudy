package ch03;

public class Operation1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte a3 = 126;
		byte b3 = 0;
		b3 = (byte)(a3 + 2);
		System.out.println(b3);
		
		int a = 10;
		int b = 1000000;
		int c = 2000000;
		long d = b*c;
		System.out.println(d);
		
		// a의 값을 음수로
		 a = -a;
		 System.out.println(a);

		 // a의 값 출력 후 증가
		 System.out.println(a++);

		 // 위의 증가된 a의 값 확인
		 System.out.println(a);

		 // a의 값 감소 후 출력
		 System.out.println(--a);
		 
		 boolean b1 = true;
		 System.out.println(b1);

		 // b의 값 논리 부정
		 b1 = !b1;
		 System.out.println(b1);
		 char c1 = 'a';
		 char c2 = c1++;
		 char c3 = ++c1;
		 System.out.println(c2);
		 System.out.println(c3);
	}

}
