package ch03;

public class Operation4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1 = 0;
		int num2 = 0;
		boolean result;

		/* Short-Circuit Evaluation */
		result = num1++ < 0 && num2++ > 0; // and연산자 : 이미 앞이 false라서 num2는 실행할 필요가 없음
		System.out.println("result : " + result);
		System.out.println("num1 : " + num1 + ", num2 : " + num2);

		result = num1++ > 0 || num2++ > 0; // or연산자 : 이미 앞에 true인 상태라 num2 실행 안함
		System.out.println("result : " + result);
		System.out.println("num1 : " + num1 + ", num2 : " + num2);
		int num3 = 0;
		int num4 = 0;
		
		/* 논리연산자 기호가 하나면 두개 다 실행함 */
		result = num3++ < 0 & num4++ > 0; 
		System.out.println("result : " + result);
		System.out.println("num3 : " + num3 + ", num4 : " + num4);

		result = num3++ > 0 | num4++ > 0;
		System.out.println("result : " + result);
		System.out.println("num3 : " + num3 + ", num4 : " + num4);

		int num5 = 0;
		num5 += 5;
		System.out.println(num5);
		
		int n1 = 10;
		int n2 = 20;
		int n3 = 30;
		
		n3 = n2 = n1;
		System.out.println(n3);
	}

}
