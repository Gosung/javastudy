package ch03;

import java.util.Scanner;

public class Remain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 123;
		int n3 = num % 10; // 3
		int n2 = (num / 10) % 10; // 12 % 10 => 2
		int n1 = (num / 10 / 10) % 10; // 1 % 10 => 1
		System.out.println(n3 + n2 + n1);
		
		for(int i=1;i<=9;i++) {
			for(int j=1;j<=9;j++) {
				System.out.print(i + "x" + j + "=" + i*j + " ");
			}
			System.out.println();
		}
		
	}
}
