package exam;

public class IfExam1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = -1;
		if (num > 10) {
			System.out.println("10보다 큽니다.");
		} else if (num > 5) {
			System.out.println("5보다 큽니다.");
		} else if (num == 5) {
			System.out.println("5와 같습니다.");
		} else {
			if (num < 0) {
				System.out.println("음수입니다.");
			} else if (num == 0) {
				System.out.println("0입니다.");
			} else {
				System.out.println("5보다 작은 양수입니다.");
			}
		}

	}

}
