package exam;

public class BreakExam3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean value = true;
		for (int i = 1; i <= 9; i++) {
			for (int j = 2; j <= 9; j++) {
				System.out.print(j + " * " + i + " = " + (j * i) + "\t");
				if(j==5) {
					continue;
				}
			}
			if(value == false) {
				break;
			}
			System.out.println();
		}
	}

}
