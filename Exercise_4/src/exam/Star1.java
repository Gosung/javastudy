package exam;

public class Star1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* 조건문 없이 */
		System.out.println("-1번-");
		for (int a = 1; a <= 5; a++) {
			for (int b = 1; b <= a; b++) {
				System.out.print("*");
			}
			System.out.println();
		}

		System.out.println("");

		/* 조건문 사용 */
		System.out.println("-2번-");
		for (int a = 1; a <= 5; a++) {
			for (int b = 5; b >= 1; b--) {
				System.out.print("*");
				if (b == a) {
					break;
				}
			}
			System.out.println();
		}
		System.out.println("");
		/* 조건문 사용 */
		System.out.println("-3번-");
		int space = 0;
		for (int a = 5; a >= 1; a--) {
			space = 5 - a;
			for (int b = 1; b <= 5; b++) {
				if (a > b) {
					System.out.print(" ");
				} else {
					System.out.print("*");
				}
			}
			System.out.println(space);
		}

		System.out.println();

		System.out.println("-4번-");
		for (int a = 5; a >= 1; a--) {
			for (int b = 5; b >= 1; b--) {
				if (a >= b) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

}
