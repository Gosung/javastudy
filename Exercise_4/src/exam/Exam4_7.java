package exam;

public class Exam4_7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 11;
		int leftSpace = num / 2;
		int rightSpace = num / 2 + 2;
		for (int row = 1; row <= num; row++) {	
			for (int col = 1; col <= num; col++) {
				if(leftSpace < col && rightSpace > col) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			if (row > num / 2) {
				leftSpace++;
				rightSpace--;
			}
			else {
				leftSpace--;
				rightSpace++;
			}
			System.out.println();
		}

		System.out.println();
	}

}
