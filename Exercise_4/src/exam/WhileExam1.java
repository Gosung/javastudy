package exam;

import java.util.Scanner;

public class WhileExam1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int total = 0;
		Scanner scan = new Scanner(System.in);
		
		System.out.print("숫자를 입력해주세요 : ");
		int num = scan.nextInt();
		while (num > 0) {
			total = total + num % 10;
			num = num / 10;
		}
		
		/* 
		while (num > 0) {
			total = total + (num % 10);
			num = / 10;
			if (num == 0) {
				break;
			}
		}
		*/

		System.out.println("각 자리 숫자의 합 : " + total);
	}

}
