package exam;

public class SwitchExam1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 1;

		switch (num) {
		case 1: case 5:
			System.out.println("[1] 1입니다.");
			break;
		case 2:
			System.out.println("[2] 2입니다.");
			break;
		default:
			System.out.println("[3] 1, 2 아님");
			break;
		}
		System.out.println("break가 없는 경우");
		switch (num) {
		case 1:
			System.out.println("[4] 1입니다.");
		case 2:
			System.out.println("[5] 여기도 실행");
		default:
			System.out.println("[6] switch 종료");
			break;
		}
	}

}
